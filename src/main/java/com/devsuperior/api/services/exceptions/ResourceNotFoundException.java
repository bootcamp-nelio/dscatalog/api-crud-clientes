package com.devsuperior.api.services.exceptions;

public class ResourceNotFoundException extends RuntimeException {

  public ResourceNotFoundException(String message) {
	super(message);
  }

}
