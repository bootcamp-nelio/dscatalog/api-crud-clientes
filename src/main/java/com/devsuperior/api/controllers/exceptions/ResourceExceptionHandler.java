package com.devsuperior.api.controllers.exceptions;

import com.devsuperior.api.services.exceptions.DatabaseException;
import com.devsuperior.api.services.exceptions.ResourceNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

  private static final String ENTITY_NOT_FOUND = "Entity not found";
  private static final String DATABASE_EXCEPTION = "Database exception";
  private static final String RESULT_SET_EXCEPTION = "ResultSet exception";

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<StandardError> entityNotFound(ResourceNotFoundException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = new StandardError(Instant.now(), status.value(), ENTITY_NOT_FOUND, e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(DatabaseException.class)
  public ResponseEntity<StandardError> databaseException(DatabaseException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = new StandardError(Instant.now(), status.value(), DATABASE_EXCEPTION, e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(EmptyResultDataAccessException.class)
  public ResponseEntity<StandardError> emptyResultDataAccessException(EmptyResultDataAccessException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = new StandardError(Instant.now(), status.value(), RESULT_SET_EXCEPTION, e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<StandardError> entityNotFoundException(EntityNotFoundException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = new StandardError(Instant.now(), status.value(), ENTITY_NOT_FOUND, e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

}
